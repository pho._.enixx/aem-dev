import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeModule } from './page/home/home.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HomeModule
  ]
})
export class AppXModule { }
